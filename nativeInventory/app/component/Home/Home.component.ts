import { Component, OnInit } from '@angular/core';
import {setString, getString} from 'application-settings'

@Component({
	selector: 'Home',
    moduleId: module.id,
	templateUrl:'./Home.component.html',
	styleUrls: ['./Home.component.css']
})

export class HomeComponent implements OnInit {
	constructor() { }
	ngOnInit() { }

}