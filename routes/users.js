const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const User = require('../models/user');
//Register
router.put('/register', (req, res, next) => {
    let newUser = new User({
        username: req.body.username,
        email: req.body.email,
        nickname: req.body.nickname,
        password: req.body.password,
        authority: {
            accessInventory: false,
            createItems: false,
            manageUsers: false
        },
        points: 0
    });

    User.getUserByUsername(newUser.username, (err,user) => {
        if(err) throw err;
        if(user) {
            res.json({success: false, msg: 'This username is taken.'});
        }
        else {
            User.addUser(newUser, (err, user) => {
                if (err) {
                    res.json({success: false, msg: err.errmsg});
                }
                else {
                    res.json({success: true, msg: 'User registered.'});
                }
            });
        }
    });

});

//Authenticate
router.post('/authenticate', (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;
    User.getUserByUsername(username, (err, user) => {
        if (err) throw err;
        if (!user) {
            return res.json({success: false, msg: 'User Not Found'});
        }

        User.comparePassword(password, user.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
                const token = jwt.sign(user, config.secret, {
                    expiresIn: 259200
                });
                res.json({
                    success: true,
                    token: 'JWT ' + token,
                    user: {
                        id: user._id,
                        username: user.username,
                        email: user.email,
                        nickname: user.nickname,
                        authority: user.authority
                    }
                });
            }
            else return res.json({success: false, msg: 'Wrong Password'});
        });
    });
});

router.get('/profile', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    res.json({user: req.user});
    console.log('finally');
});


module.exports = router;