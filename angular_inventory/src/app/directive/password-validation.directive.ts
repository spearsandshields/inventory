import { Directive } from '@angular/core';
import {AbstractControl, ValidatorFn} from "@angular/forms";

export function validatePassword(control: AbstractControl): {[key: string]: any} {
    if(control.parent == null || control.parent.get('passwordToConfirm') == null || control.parent.get('password') == null) return null;
    let password: string = control.parent.get('password').value;
    let passwordToConfirm: string = control.parent.get('passwordToConfirm').value;
    let returnObject : any = password !== passwordToConfirm? {invalidPassword: {valid: false}} : null;
    return returnObject;
}

@Directive({
  selector: '[appPasswordValidation]'
})

export class PasswordValidationDirective {

  constructor() { }

}
