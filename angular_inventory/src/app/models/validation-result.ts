export class ValidationResult {
  success : boolean;
  errorMessage?: String;
}
