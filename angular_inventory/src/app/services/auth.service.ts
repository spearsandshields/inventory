import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {UserLogin} from "../models/user-login";
import {UserRegister} from "../models/user-register";
import { tokenNotExpired } from "angular2-jwt";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class AuthService {
  private authtoken: any;
  user: any;
  isLoggedIn : BehaviorSubject<boolean> = new BehaviorSubject(this.loggedIn());
  constructor(private http: HttpClient) { }

  registerUser(user: UserRegister): Observable<any> {
    return this.http.put('http://localhost:3000/users/register', user);
  }

  authenticateUser(user: UserLogin): Observable<any> {
    return this.http.post('http://localhost:3000/users/authenticate', user);
  }

  loggedIn(): boolean {
    return tokenNotExpired('id_token');
  }

  getProfile(): Observable<any> {
    this.loadToken();
    return this.http.get('http://localhost:3000/users/profile', {headers: new HttpHeaders().set('Authorization', this.authtoken)});

  }

  loadToken(): void {
    const token = localStorage.getItem('id_token');
    this.authtoken = token;

  }

  storeTokenData(token, user: any) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authtoken = token;
    this.user = user;
    this.isLoggedIn.next(true);
  }

  logout() {
    this.authtoken = null;
    this.user = null;
    localStorage.clear();
    this.isLoggedIn.next(false);
  }
}
