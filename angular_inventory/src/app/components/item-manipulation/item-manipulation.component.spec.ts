import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemManipulationComponent } from './item-manipulation.component';

describe('ItemManipulationComponent', () => {
  let component: ItemManipulationComponent;
  let fixture: ComponentFixture<ItemManipulationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemManipulationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemManipulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
