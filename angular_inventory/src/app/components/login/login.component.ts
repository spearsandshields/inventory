import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { UserLogin} from "../../models/user-login";
import { FlashMessagesService } from "angular2-flash-messages";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router : Router,
              private flashmessage: FlashMessagesService) {
    this.createForm();
  }

  createForm(): void {
    let emailPattern: string = '^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$';
    let passwordPattern: string ='^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$';
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onSubmit(): void {
    const formModel = this.loginForm.value;
    let user: UserLogin = {
      username: formModel.username,
      password: formModel.password
    };
    this.authService.authenticateUser(user).subscribe((data)=>{
      if(data.success) {
        this.authService.storeTokenData(data.token, data.user);
        this.flashmessage.show("Welcome, "+ data.user.username, {
          cssClass: 'text-center flash-message alert alert-success ',
          timeout: 3000
        });
        this.router.navigate(['/']);

      }
    });
  }

  ngOnInit() {
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

}
