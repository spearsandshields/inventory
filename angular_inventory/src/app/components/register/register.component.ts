import {Component, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserRegister } from '../../models/user-register';

import { AuthService } from "../../services/auth.service";
import { validatePassword } from '../../directive/password-validation.directive'
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  formResponse: object;
  postRegister: boolean = false;

  constructor(private fb: FormBuilder,
              private authService: AuthService) {
    this.createForm();
  }

  createForm(): void {
    let emailPattern: string = '^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$';
    let passwordPattern: string ='^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$';
    this.registerForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', Validators.pattern(emailPattern)],
      nickname:['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.pattern(passwordPattern)]],
      passwordToConfirm:['', [Validators.required, validatePassword]]
    });
  }

  checkResponse(response: any) : void {
    console.log(response);
    if(response.success){
      this.registerForm.reset();
      this.postRegister = true;
    }
    this.formResponse = response;
  }

  ngOnInit() {
  }

  onBlur(valid: boolean): void {
    if(valid && this.registerForm.value.nickname === '') {
      this.registerForm.patchValue({nickname: this.registerForm.value.username});
    }
  }

  onSubmit() : void {
    const formModel = this.registerForm.value;
    let user: UserRegister = {
      username: formModel.username,
      email: formModel.email,
      password: formModel.password,
      nickname: formModel.nickname
    };
    let observable : Observable<any> = this.authService.registerUser(user);
    observable.subscribe( (data) => this.checkResponse(data));
  }

  get username() {
    return this.registerForm.get('username');
  }

  get email() {
    return this.registerForm.get('email');
  }

  get password() {
    return this.registerForm.get('password');
  }

  get passwordToConfirm() {
    return this.registerForm.get('passwordToConfirm');
  }

  get nickname() {
    return this.registerForm.get('nickname');
  }


}
