import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  openItemBox: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  toggle() :void{
    this.openItemBox = !this.openItemBox;
  }
}
